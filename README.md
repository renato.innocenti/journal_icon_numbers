# Automatic Journal Icon Numbers

This module will automatically apply numbered icons (map pins) to journal entries that start with a number.  It supports the following numbering formats automatically, with both upper and lower cases.

* [0-99]
* [A-Z][0-99]
* [0-99][A-Z]

![Example of assorted pins on a map](example.png)

It is also now possible to manually specify arbitrary text in an icon; though a max of 3 characters is recommended for legibility, but 4 may work in some cases. You can also disable the mod on specific pins if you wish to use the stock icons (or anything via Pin Cushion).


To use, just drag correctly named journals to your map. The icon will be automatically selected to match the name. Alternatively, you can manually enter the icon text in the Map Note Configuration window.



You can select a default global style of shape, color and font in the settings menu, and also override all of thm on a per-pin basis.

The above screenshot uses the [Backgroundless Pins](https://foundryvtt.com/packages/backgroundless-pins/) mod which is highly recommended.


## Known issues
* None
